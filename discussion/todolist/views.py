from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.template import loader
from django.contrib.auth import authenticate, login, logout
from django.forms.models import model_to_dict

# We import the build-in 'User' model to be able to do operations to the built-in Users table from Django
from django.contrib.auth.models import User
from .models import ToDoItem

# Create your views here.
def index(request):
	todoitem_list = ToDoItem.objects.all()

	template = loader.get_template('todolist/index.html')
	context = {
		'todoitem_list': todoitem_list,
		'user': request.user
	}

	return render(request,"todolist/index.html",context)

	# you can also render a template using this way
	#return HttpResponse(template.render(context, request))

def todoitem(request, todoitem_id):
	# 'model_to_dict' funtion translates a data model into a python dictionary(object). This is so that we can use the data as a regular object/dictionary
	todoitem = model_to_dict(ToDoItem.objects.get(pk=todoitem_id))
	return render(request, "todolist/todoitem.html", todoitem)


def register(request):
	# retrieving all the user
	users = User.objects.all()
	is_user_registered = False 

	for individual_user in users:
		if individual_user.username == "johndoe":
			is_user_registered = True
			break

	#The context 'is_user_registered' is put after the for loop in order for it to get the updated value if 'for' loop above equals 'True'
	context = {
		'is_user_registered': is_user_registered
	}

	if is_user_registered == False:

		# 1. Creating a new instance of the User model.
		user = User()

		# 2. Assign data values to each property of the model
		user.username = "johndoe"
		user.first_name = "John"
		user.last_name = "Doe"
		user.email = "john@mail.com"
		user.set_password("john1234")
		user.is_staff = False
		user.is_active = True

		# 3. Save the new instance of user into the database
		user.save()

		# The context will be the data to be passed to the template
		context = {
			"first_name": user.first_name,
			"last_name": user.last_name
		}

	return render(request,"todolist/register.html",context)

def change_password(request):
	is_user_auth = False

	user = authenticate(username="johndoe", password="john1234")

	print(user)

	if user is not None:
		authenticated_user = User.objects.get(username="johndoe")
		authenticated_user.set_password("johndoe1")
		authenticated_user.save()

		is_user_auth = True

	context = {
		"is_user_auth": is_user_auth
	}

	return render(request, "todolist/change_password.html", context)

def login_view(request):
	username = "johndoe"
	password = "john1234"

	user = authenticate(username=username, password=password)

	context = {
		"is_user_auth": False
	}

	if user is not None:
		# The 
		login(request, user)
		return redirect("index")
	else:
		return render(request, "todolist/login.html", context)

def logout_view(request):
	logout(request)
	return redirect("index")
